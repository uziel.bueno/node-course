const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendWelcomeEmail = (email, name) => {
  return sgMail.send({
    to: email,
    from: 'uziel@naobatec.com',
    subject: 'Thanks for joining in!',
    text: `Welcome to Task App ${name}. Let me know how you get along with the app.`,
    html: '',
  });
};

const sendCancelationEmail = (user) => {
  sgMail.send({
    to: user.email,
    from: 'uziel@naobatec.com',
    subject: 'Your Task App account is cancelled',
    text: `
    ${user.name}, we are sad to see you go.
    I'm just confirming that your Task App account has been canceled.
    I hope you decide to come back soon and enjoy all the features we've made for you with love.`,
  });
};

module.exports = {
  sendWelcomeEmail,
  sendCancelationEmail,
};
