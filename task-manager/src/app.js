const express = require('express');
require('./db/mongoose');
const requestImprovements = require('./middleware/request-improvements');
const authRouter = require('./routers/auth');
const userRouter = require('./routers/user');
const taskRouter = require('./routers/task');

const app = express();

app.use(express.json());
app.use(requestImprovements);

// Routes
app.use(authRouter);
app.use(userRouter);
app.use(taskRouter);

app.get('*', (req, res) => {
  res.status(404).end();
});

module.exports = app;
