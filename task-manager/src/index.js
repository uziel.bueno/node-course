const app = require('./app');
const port = process.env.PORT || 3000;
// const mongoose = require('mongoose');

const server = app.listen(port, () =>
  console.log(`Server is listening on port: ${port}`)
);

process.on('SIGTERM', async () => {
  await mongoose.connection.close();

  server.close(() => {
    console.log('Process gracefully terminated.');
  });
});
