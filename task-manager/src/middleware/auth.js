const jwt = require('jsonwebtoken');
const User = require('../models/user');

module.exports = async (req, res, next) => {
  try {
    // check for auth header
    // extract token from header value
    const token = req.headers.authorization.replace('Bearer ', '');
    // verify token
    const payload = jwt.verify(token, process.env.JWT_SECRET);
    const user = await User.findOne({
      _id: payload._id,
      'tokens.token': token,
    });

    if (!user) {
      throw new Error();
    }

    req.token = token;
    req.user = user;

    // call next middleware
    next();
  } catch (error) {
    res.status(401).send({ error: 'Please authenticate.' });
  }
};
