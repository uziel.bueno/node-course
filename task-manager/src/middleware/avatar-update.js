const multer = require('multer');

const fileFilter = function(req, file, cb) {
  const onlyImagesRegex = /\.(jpeg|jpg|png)$/;

  if (!file.originalname.match(onlyImagesRegex)) {
    return cb(new Error('File must be an image.'));
  }

  cb(undefined, true);
};

module.exports = multer({
  limits: { fileSize: 1000000 },
  fileFilter,
});
