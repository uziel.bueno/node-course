const requestImprovements = (req, res, next) => {
  req.only = function(keys = []) {
    if (arguments.length > 1) {
      keys = Array.from(arguments);
    }

    return Object.keys(this.body)
      .filter((key) => keys.includes(key))
      .reduce((filtered, key) => {
        filtered[key] = this.body[key];
        return filtered;
      }, {});
  };

  next();
};

module.exports = requestImprovements;
