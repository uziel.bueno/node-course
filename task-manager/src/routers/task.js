const express = require('express');
const mongoose = require('mongoose');
const auth = require('../middleware/auth');
const Task = require('../models/task');
const router = new express.Router();

router.post('/tasks', auth, async (req, res) => {
  const task = new Task({
    ...req.only('description', 'completed'),
    owner: req.user._id,
  });

  try {
    await task.save();

    res.status(201).json(task);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.get('/tasks', auth, async (req, res) => {
  const match = {};
  const sort = {};

  if (req.query.completed) {
    match.completed = req.query.completed == 'true' ? true : false;
  }

  if (req.query.search) {
    match.description = { $regex: req.query.search, $options: 'i' };
  }

  if (req.query.sort) {
    const [path, direction] = req.query.sort.split(':');

    if (direction === 'asc') sort[path] = 1;
    if (direction === 'desc') sort[path] = -1;
  }

  try {
    await req.user
      .populate({
        path: 'tasks',
        match,
        options: {
          limit: parseInt(req.query.limit, 10),
          skip: parseInt(req.query.skip, 10),
          sort,
        },
      })
      .execPopulate();

    res.status(200).json(req.user.tasks);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.get('/tasks/:id', auth, async (req, res) => {
  const _id = req.params.id;

  if (!mongoose.isValidObjectId(_id)) {
    return res.status(404).json({ error: 'The given task was not found!' });
  }

  try {
    const task = await Task.findOne({ owner: req.user._id, _id });

    if (!task) {
      return res.status(404).json({ error: 'The given task was not found!' });
    }

    res.status(200).json(task);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.patch('/tasks/:id', auth, async (req, res) => {
  const _id = req.params.id;

  if (!mongoose.isValidObjectId(_id)) {
    return res.status(404).json({ error: 'The given task was not found!' });
  }

  try {
    const task = await Task.findOne({ _id, owner: req.user._id });

    if (!task) {
      return res.status(404).json({ error: 'The given task was not found!' });
    }

    const updates = req.only('description', 'completed');
    Object.keys(updates).forEach((update) => (task[update] = updates[update]));

    await task.save();

    res.status(200).json(task);
  } catch (error) {
    if (error instanceof mongoose.Error.ValidationError) {
      return res.status(422).json({ error: error.message });
    }
    res.status(500).json({ error: error.message });
  }
});

router.delete('/tasks/:id', auth, async (req, res) => {
  const _id = req.params.id;

  if (!mongoose.isValidObjectId(_id)) {
    return res.status(404).json({ error: 'The given task was not found!' });
  }

  try {
    const task = await Task.findOneAndDelete({ _id, owner: req.user._id });

    if (!task) {
      return res.status(404).json({ error: 'The given task was not found!' });
    }

    res.status(204).json(null);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

module.exports = router;
