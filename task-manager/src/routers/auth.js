const express = require('express');
const auth = require('../middleware/auth');
const User = require('../models/user');
const router = new express.Router();

router.post('/auth/tokens', async (req, res) => {
  try {
    const { email, password } = req.only('email', 'password');

    const user = await User.findByCredentials(email, password);
    const token = await user.generateAuthToken();

    res.status(201).send({ user, token });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

router.post('/auth/logout', auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter(
      (token) => token.token !== req.token
    );

    await req.user.save();

    res.end();
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});

router.delete('/auth/tokens', auth, async (req, res) => {
  try {
    req.user.tokens = [];

    await req.user.save();

    res.status(204).end();
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});

module.exports = router;
