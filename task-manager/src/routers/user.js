const express = require('express');
const mongoose = require('mongoose');
const sharp = require('sharp');
const auth = require('../middleware/auth');
const avatar = require('../middleware/avatar-upload');
const User = require('../models/user');
const { sendWelcomeEmail, sendCancelationEmail } = require('../emails/account');

const router = new express.Router();

router.post('/users', async (req, res) => {
  const user = new User(req.only('name', 'age', 'email', 'password'));

  try {
    await user.save();

    sendWelcomeEmail(user.email, user.name);

    const token = await user.generateAuthToken();

    res.status(201).send({ user, token });
  } catch (error) {
    res.status(400).send(error);
  }
});

router.get('/users/me', auth, async (req, res) => {
  try {
    res.send(req.user);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.patch('/users/me', auth, async (req, res) => {
  try {
    const updates = req.only(['name', 'password', 'age']);

    Object.keys(updates).forEach(
      (update) => (req.user[update] = updates[update])
    );

    await req.user.save();

    res.status(200).json(req.user);
  } catch (error) {
    if (error instanceof mongoose.Error.ValidationError) {
      return res.status(422).send(error);
    }
    res.status(500).send({ error: error.message });
  }
});

router.delete('/users/me', auth, async (req, res) => {
  try {
    const user = await req.user.remove();

    sendCancelationEmail(user);

    res.status(204).send();
  } catch (error) {
    console.log(error);
    res.status(500).send({ error: error.message });
  }
});

router.post(
  '/users/me/avatar',
  auth,
  avatar.single('file'),
  async (req, res) => {
    if (!req.file) {
      return res.status(422).send({ error: 'File is required.' });
    }

    const buffer = await sharp(req.file.buffer)
      .resize(250, 250)
      .png()
      .toBuffer();
    req.user.avatar = buffer;

    await req.user.save();

    res.status(200).json();
  },
  (error, req, res, next) => {
    res.status(400).json({ error: error.message });
  }
);

router.delete('/users/me/avatar', auth, async (req, res) => {
  req.user.avatar = undefined;

  await req.user.save();

  res.status(204).json(null);
});

router.get('/users/:id/avatar', async (req, res) => {
  const _id = req.params.id;

  if (!mongoose.isValidObjectId(_id)) {
    return res.status(404).json('The given user was not found.');
  }

  try {
    const user = await User.findById(_id);

    if (!user && !user.avatar) {
      return res.status(404).json('The given user was not found.');
    }

    res.set('Content-Type', 'image/png');
    res.send(user.avatar);
  } catch (error) {
    res.status(404).json(null);
  }
});

module.exports = router;
