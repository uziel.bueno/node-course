const mongoose = require('mongoose');

const connectionUrl = process.env.MONGODB_URL;

// mongoose.set('debug', true);

// Connect to database
const connection = mongoose
  .connect(connectionUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  })
  .catch((error) => console.log('Unable to connect to database!'));
