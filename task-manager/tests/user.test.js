const request = require('supertest');
const app = require('../src/app');
const User = require('../src/models/user');
const { userOneId, userOne, setupDatabase, tearDownDatabase } = require('./fixtures/db');

beforeEach(setupDatabase);
afterAll(tearDownDatabase);

describe('Authentication', () => {
  test('Should login existing user', async () => {
    const response = await request(app)
      .post('/auth/tokens')
      .send({
        email: userOne.email,
        password: userOne.password,
      })
      .expect(201);

    const user = await User.findById(userOne._id);
    expect(user.tokens.length).toBe(2);
    expect(response.body.token).not.toBeNull();
    expect(response.body.token).toBe(user.tokens[1].token);
  });

  test('Should not login nonexistent user', async () => {
    await request(app)
      .post('/auth/tokens')
      .send({
        email: 'invalid@example.com',
        password: 'fakepass',
      })
      .expect(400);
  });
});

describe('Users', () => {
  test('Should signup a new user', async () => {
    //   mail.sendWelcomeEmail.mockResolvedValue(Promise.resolve('ok'));

    const response = await request(app)
      .post('/users')
      .send({
        name: 'Uziel',
        email: 'uziel@naobatec.com',
        password: 'p@ssword!',
      })
      .expect(201);

    const user = await User.findById(response.body.user._id);
    expect(user).not.toBeNull();
    expect(user.password).not.toBe('p@assword');

    expect(response.body).toMatchObject({
      user: {
        name: 'Uziel',
        email: 'uziel@naobatec.com',
      },
      token: user.tokens[0].token,
    });
  });

  test('Should get profile for authenticated user', async () => {
    await request(app)
      .get('/users/me')
      .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
      .send()
      .expect(200);
  });

  test('Should not get profile for unauthenticated user', async () => {
    await request(app)
      .get('/users/me')
      .send()
      .expect(401);
  });

  test('Should delete account for authenticated user', async () => {
    await request(app)
      .delete('/users/me')
      .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
      .send()
      .expect(204);

    const user = await User.findById(userOne._id);
    expect(user).toBeNull();
  });

  test('Should not delete account for unauthenticated user', async () => {
    await request(app)
      .delete('/users/me')
      .send()
      .expect(401);

    const user = await User.findById(userOne._id);
    expect(user).not.toBeNull();
  });

  test('Should upload avatar image', async () => {
    const response = await request(app)
      .post('/users/me/avatar')
      .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
      .attach('file', 'tests/fixtures/profile-pic.jpg')
      .expect(200);

    const user = await User.findById(userOne._id);
    expect(user.avatar).toEqual(expect.any(Buffer));
  });

  test('Should update valid user fields', async () => {
    const response = await request(app)
      .patch('/users/me')
      .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
      .send({
        name: 'John Doe',
        age: 34,
        password: 'mynewpass',
      })
      .expect(200);

    const user = await User.findById(userOne._id);
    expect(user.name).toBe('John Doe');
    expect(user.age).toBe(34);
    expect(user.password).not.toBe(userOne.password);
  });

  test('Should not update password if contains the word "password"', async () => {
    const response = await request(app)
      .patch('/users/me')
      .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
      .send({
        password: 'mynewpassword',
      })
      .expect(422);
  });

  test('Should not update invalid user fields', async () => {
    const response = await request(app)
      .patch('/users/me')
      .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
      .send({
        email: 'johndoe@example.com',
      })
      .expect(200);

    expect(response.body.email).not.toBe('johndoe@example.com');
  });
});
