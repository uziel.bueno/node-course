require('../src/db/mongoose');

const User = require('../src/models/user');

// 5e6b3f700f17702cb37a5d50

// User.findByIdAndUpdate('5e6b34563260ea7916ca70c4', { age: 1 })
//   .then((user) => {
//     console.log(user);
//     return User.countDocuments({ age: 1 });
//   })
//   .then((count) => {
//     console.log(count);
//   })
//   .catch((e) => console.log(e));

const updateAgeAndCount = async (id, age) => {
  const user = await User.findByIdAndUpdate(id, { age });
  const count = await User.countDocuments({ age });

  return count;
};

updateAgeAndCount('5e6b3f700f17702cb37a5d50', 2)
  .then((count) => {
    console.log(count);
  })
  .catch((e) => console.log(e));
