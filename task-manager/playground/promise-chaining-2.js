require('../src/db/mongoose');
const Task = require('../src/models/task');

// Task.findByIdAndDelete('5e6b4226497d3935cc1d7941')
//   .then((removedTask) => {
//     console.log({ removedTask });

//     return Task.countDocuments({ completed: false });
//   })
//   .then((uncopletedCount) => {
//     console.log({ uncopletedCount });
//   });

const deleteTaskAndCount = async (id) => {
  const deletedTask = await Task.findByIdAndDelete(id);
  const uncompletedCount = Task.find({ completed: false });

  return uncompletedCount;
};

deleteTaskAndCount('5e6b41d9497d3935cc1d793e')
  .then((count) => {
    console.log(count);
  })
  .catch((e) => console.log(error));
