// CRUD create read update delete

// const mongodb = require('mongodb');
// const MongoClient = mongodb.MongoClient;
// const ObjectID = mongodb.ObjectID;
const { MongoClient, ObjectID } = require('mongodb');

const connectionURL = 'mongodb://127.0.0.1:27017';
const databaseName = 'task-manager';

const id = new ObjectID();
// console.log(id.id); // -> Binary data of the id <Buffer>
// console.log(id.id.length); // -> Length of binary data <12>
// console.log(id.toHexString().length); // -> Hex/String representation of the id
// console.log(id.getTimestamp()); // -> Get the embedded timestamp for the id

MongoClient.connect(
  connectionURL,
  { useNewUrlParser: true, useUnifiedTopology: true },
  (error, client) => {
    if (error) {
      return console.log(error);
    }

    const db = client.db(databaseName);

    db.collection('tasks')
      .deleteOne({ description: 'Take AWS Course' })
      .then((result) =>
        console.log({
          matchedCount: result.matchedCount,
          deletedCount: result.deletedCount,
        })
      )
      .catch((error) => console.log(error));

    // db.collection('users')
    //   .deleteMany({ age: 27 })
    //   .then((result) =>
    //     console.log({
    //       matchedCount: result.matchedCount,
    //       deletedCount: result.deletedCount,
    //     })
    //   )
    //   .catch((error) => console.log(error));
  }
);

// db.collection('tasks')
//   .updateMany(
//     { completed: false },
//     {
//       $set: {
//         completed: true,
//       },
//     }
//   )
//   .then((result) =>
//     console.log({
//       matchedCount: result.matchedCount,
//       modifiedCount: result.modifiedCount,
//     })
//   )
//   .catch((error) => console.log(error));

// db.collection('users')
//   .updateOne(
//     { _id: new ObjectID('5e6b0b4ff628d28bad71ac93') },
//     {
//       $inc: {
//         age: 1,
//       },
//     }
//   )
//   .then((result) =>
//     console.log({
//       matchedCount: result.matchedCount,
//       modifiedCount: result.modifiedCount,
//     })
//   )
//   .catch((error) => console.log(error));

// db.collection('tasks').findOne(
//   { _id: new ObjectID('5e6b0c4be2218c9241b428cd') },
//   (error, task) => {
//     if (error) {
//       return console.log('Unable to fetch the task');
//     }

//     console.log(task);
//   }
// );

// db.collection('tasks')
//   .find({ completed: false })
//   .toArray((error, tasks) => {
//     if (error) {
//       return console.log('Unable to fetch tasks');
//     }

//     console.log(tasks);
//   });

// db.collection('users').findOne(
//   { _id: new ObjectID('5e6b09b9812db88546ee74ff') },
//   (error, user) => {
//     if (error) {
//       return console.log('Unable to fetch the user');
//     }

//     // If no documents are found, null is returned;
//     console.log(user);
//   }
// );

// db.collection('users')
//   .find({ age: 27 })
//   .toArray((error, users) => {
//     console.log(users);
//   });

// db.collection('users')
//   .find({ age: 27 })
//   .count((error, count) => {
//     console.log(count);
//   });

// db.collection('users').insertOne(
//   {
//     name: 'Vikram',
//     age: 26,
//   },
//   (error, result) => {
//     if (error) {
//       return console.log('Unable to insert user.');
//     }

//     console.log(result.ops);
//   }
// );

// db.collection('users').insertMany(
//   [
//     {
//       name: 'Jen',
//       age: 28,
//     },
//     {
//       name: 'Andrew',
//       age: 29,
//     },
//   ],
//   (error, result) => {
//     if (error) {
//       return console.log('Unable to insert users!');
//     }

//     console.log(result.ops);
//   }
// );

// db.collection('tasks').insertMany(
//   [
//     { description: 'Take React Course', completed: true },
//     { description: 'Take Node.js Course', completed: false },
//     { description: 'Take AWS Course', completed: false },
//   ],
//   (error, result) => {
//     if (error) {
//       return console.log('Unable to insert tasks');
//     }

//     console.log(result.ops);
//   }
// );
