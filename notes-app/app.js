const chalk = require('chalk');
const yargs = require('yargs');
const notes = require('./notes');

yargs.version('1.1.0');

// Create add command
yargs.command({
  command: 'add',
  describe: 'Add a new note.',
  builder: {
    title: {
      describe: 'Note title',
      demandOption: true,
      type: 'string',
    },
    body: {
      describe: 'Note body',
      demandOption: true,
      type: 'string',
    },
  },
  handler(argv) {
    notes.addNote(argv.title, argv.body);
  },
});

// Create remove command
yargs.command({
  command: 'remove',
  describe: 'Remove a note.',
  builder: {
    title: {
      describe: 'The title of the note to remove.',
      demandOption: true,
      type: 'string',
    },
  },
  handler(argv) {
    notes.removeNote(argv.title);
  },
});

// Create list command
yargs.command({
  command: 'list',
  describe: 'List the notes.',
  handler(argv) {
    const noteList = notes.listNotes();

    console.log(chalk.blue('Your notes:'));

    noteList.forEach((note) => {
      console.log(chalk.cyanBright(note.title));
    });
  },
});

// Create read command
yargs.command({
  command: 'read',
  describe: 'Reading a note.',
  builder: {
    title: {
      describe: 'The title of a note to read.',
      demandOption: true,
      type: 'string',
    },
  },
  handler(argv) {
    try {
      const note = notes.readNote(argv.title);
      console.log(chalk.magenta(note.title));
      console.log(chalk.dim(note.body));
    } catch (error) {
      console.log(chalk.white.bgRed.bold(error.message));
    }
  },
});

yargs.parse();

// const command = process.argv[2];
// const params = {};
// const args = process.argv
//   .slice(3)
//   .filter(function(arg) {
//     return /^--\S+=(.+)$/gi.test(arg);
//   })
//   .map(function(arg) {
//     return /^--(\S+)=(.+)$/gi.exec(arg).slice(1);
//   })
//   .forEach(function(arg) {
//     params[arg[0]] = arg[1];
//   });

// if (command === 'add') {
//   const { title, name } = params;

//   console.log(`Adding note! title: ${title}, name: ${name}`);
// }

// if (command === 'remove') {
//   console.log('Removing note!');
// }

// const add = require('./utils');
// const sum = add(4, -1);
// console.log({ sum });

// const fs = require('fs');
// const path = require('path');
// fs.writeFileSync(path.join(__dirname, 'notes.txt'), 'My name is Uziel.\n');
// fs.appendFileSync(
//   path.join(__dirname, 'notes.txt'),
//   "I'm Learning Node.js with Andrew."
// );
