const fs = require('fs');
const chalk = require('chalk');

// GET NOTES
const listNotes = () => {
  return loadNotes();
};

// READ NOTE
const readNote = (title) => {
  const notes = loadNotes();

  const note = notes.find((note) => note.title === title);

  if (!note) {
    throw new Error('A note with the given title does not exist');
  }

  return note;
};

// ADD NOTE
const addNote = (title, body) => {
  const notes = loadNotes();
  const duplicateNote = notes.find((note) => note.title === title);

  if (duplicateNote) {
    console.log(
      chalk.red.inverse('A note with the same title already exists!')
    );
    return;
  }

  notes.push({ title: title, body: body });
  saveNotes(notes);
  console.log(chalk.green.inverse('New note added!'));
};

// REMOVE NOTE
const removeNote = (title) => {
  const notes = loadNotes();
  const newNotes = notes.filter((note) => note.title !== title);

  if (newNotes.length === notes.length) {
    console.log(chalk.red.inverse('No note found!'));
    return;
  }

  saveNotes(newNotes);
  console.log(chalk.green.inverse('Note removed!'));
};

const saveNotes = (notes) => {
  const dataJSON = JSON.stringify(notes, null, ' ');
  // debugger;
  fs.writeFileSync('notes.json', dataJSON);
};

const loadNotes = () => {
  try {
    const dataBuffer = fs.readFileSync('notes.json');
    const dataJSON = dataBuffer.toString();

    return JSON.parse(dataJSON);
  } catch (e) {
    return [];
  }
};

// Exporting multiple things...
module.exports = {
  listNotes: listNotes,
  addNote: addNote,
  removeNote: removeNote,
  readNote: readNote,
};
