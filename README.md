# The Complete Node.js Developer Course (3rd Edition)

## Debugging Tools

- [Node.js Debugging in VSCode](https://code.visualstudio.com/docs/nodejs/nodejs-debugging)
- [Nodemon VSCode](https://github.com/microsoft/vscode-recipes/tree/master/nodemon)
- [Docker Typescript VSCode](https://github.com/microsoft/vscode-recipes/tree/master/Docker-TypeScript)

## Node.js with Typescript

- [How to develop a Node.js project in Typescript](https://www.quora.com/What-language-is-better-to-write-a-Node-app-JS-or-TypeScript)
