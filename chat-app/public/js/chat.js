const socket = io();

const messageForm = document.querySelector("#message-form");
const sendLocationButton = document.querySelector("#send-location");
const sendMessageButton = document.querySelector("#send-message");
const messages = document.querySelector("#messages");
const sidebar = document.querySelector("#sidebar");

// Templates
const messageTemplate = document.querySelector("#message-template").innerHTML;
const locationMessageTemplate = document.querySelector(
  "#location-message-template"
).innerHTML;
const sideBarTemplate = document.querySelector("#sidebar-template").innerHTML;

//Options
let { username, room } = Qs.parse(location.search, {
  ignoreQueryPrefix: true,
});

username = username.trim().toLowerCase();
room = room.trim().toLowerCase();

function resolveUsernameForMessage(name) {
  return username === name ? "You" : name;
}

const autoscroll = () => {
  // New Message Element
  const $newMessage = messages.lastElementChild;

  // height of the new message
  const newMessageStyles = getComputedStyle($newMessage);
  const newMessageMargin = parseInt(newMessageStyles.marginBottom, 10);
  const newMessageHeight = $newMessage.offsetHeight + newMessageMargin;

  // Visible Height
  const visibleHeight = messages.offsetHeight;
  // Height of message container
  const containerHeight = messages.scrollHeight;
  // How far have I scrolled?
  const scrollOffset = messages.scrollTop + visibleHeight;

  if (containerHeight - newMessageHeight <= scrollOffset) {
    messages.scrollTop = messages.scrollHeight;
  }
};

socket.on("message", (message) => {
  renderMessage(
    {
      username: resolveUsernameForMessage(message.username),
      message: message.text,
      createdAt: moment(message.createdAt).format("h:mm a"),
    },
    messageTemplate
  );
});

socket.on("locationMessage", (message) => {
  renderMessage(
    {
      username: resolveUsernameForMessage(message.username),
      url: message.url,
      createdAt: moment(message.createdAt).format("h:mm a"),
    },
    locationMessageTemplate
  );
});

socket.on("roomData", ({ room, users }) => {
  const html = Mustache.render(sideBarTemplate, { room, users });

  sidebar.innerHTML = html;
});

messageForm.addEventListener("submit", (e) => {
  e.preventDefault();
  sendMessageButton.setAttribute("disabled", true);

  const input = e.target.elements["message"];
  const message = input.value;

  socket.emit("sendMessage", message, (error) => {
    sendMessageButton.removeAttribute("disabled");
    input.value = "";
    input.focus();

    if (error) {
      return console.log(error);
    }

    console.log("Message delivered!");
  });
});

sendLocationButton.addEventListener("click", (e) => {
  if (!navigator.geolocation) {
    return console.log("Your browser does not support geolocation");
  }

  e.target.setAttribute("disabled", true);

  navigator.geolocation.getCurrentPosition(sendLocation, (error) => {
    e.target.removeAttribute("disabled");
    console.log(error);
  });
});

function sendLocation(position) {
  const location = {
    latitude: position.coords.latitude,
    longitude: position.coords.longitude,
  };

  socket.emit("sendLocation", location, (error) => {
    sendLocationButton.removeAttribute("disabled");

    if (error) {
      return console.log(error);
    }

    console.log("Your location was shared successfully!");
  });
}

function renderMessage(message, template) {
  const html = Mustache.render(template, message);
  messages.insertAdjacentHTML("beforeend", html);
  autoscroll();
}

socket.emit("join", { username, room }, (error) => {
  if (error) {
    alert(error);
    location.href = "/";
  }
});
