const generateMessage = (user, message) => ({
  userId: user.id,
  username: user.username,
  text: message,
  createdAt: new Date().getTime(),
});

const generateLocationMessage = (user, coords) => ({
  userId: user.id,
  username: user.username,
  url: `https://google.com/maps?q=${coords.latitude},${coords.longitude}`,
  createdAt: new Date().getTime(),
});

module.exports = {
  generateMessage,
  generateLocationMessage,
};
