// Promises
// const doWorkPromise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     reject('An error ocurred!');
//     // resolve('This is your data');
//   }, 2000);
// });

// doWorkPromise
//   .then((data) => console.log(data))
//   .catch((error) => console.log(error));

// // Async/Await
// async function main() {
//   const data = await doWorkAsync();

//   console.log(data);
// }

// const doWorkAsync = () => {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       resolve([1, 2, 3]);
//     }, 2000);
//   });
// };

// main();

const add = (a, b) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(a + b);
    }, 2000);
  });
};

// add(1, 2)
//   .then((sum) => {
//     console.log(sum);

//     add(4, 4)
//       .then((sum2) => {
//         console.log(sum2);
//       })
//       .catch((e) => {
//         console.log(e);
//       });
//   })
//   .catch((e) => {
//     console.log(e);
//   });

add(1, 2)
  .then((sum) => {
    console.log(sum);

    return add(4, 4);
  })
  .then((sum2) => {
    console.log(sum2);
  })
  .catch((e) => {
    console.log(e);
  });
