const fs = require('fs');
const path = require('path');

// Read file data...
const dataBuffer = fs.readFileSync(path.join(__dirname, '1-json.json'));
const dataJSON = dataBuffer.toString();
const data = JSON.parse(dataJSON);

// Change read data...
data.name = 'Uziel';
data.age = 34;

// Write new data on the file...
fs.writeFileSync(path.join(__dirname, '1-json.json'), JSON.stringify(data));

// const book = {
//   title: 'Ego is the enemy',
//   author: 'Ryan Holiday',
// };

// const bookJSON = JSON.stringify(book);

// Read buffer...
// const dataBuffer = fs.readFileSync(path.join(__dirname, '1-json.json'));
// Convert buffer data to string...
// const dataJSON = dataBuffer.toString();
// Parse string data to JSON...
// const data = JSON.parse(dataJSON);

// console.log(data.title);
// fs.writeFileSync(path.join(__dirname, '1-json.json'), bookJSON);
