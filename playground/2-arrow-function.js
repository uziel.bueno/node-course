// const square = function(number) {
//   return number * number;
// };

// const square = (number) => {
//   return number * number;
// };

// const square = (number) => number * number;

// console.log(square(3));

const event = {
  name: 'Birthday Party',
  guestList: ['Uziel', 'Sofia', 'Winnie', 'Cristina', 'Jacob'],
  // ES6 Method Definition Syntax. Appropiate for methods on objects.
  printGuestList() {
    console.log('Guest list for: ' + this.name);

    // Arrow functions don't bind their own "this" value, they take their parent "this".
    this.guestList.forEach((guest) =>
      console.log(guest + ' is attending ' + this.name)
    );
  },
};

console.log(event.printGuestList());
