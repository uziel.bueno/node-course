const https = require('https');

const url =
  'https://api.darksky.net/forecast/42efd423d0580866b886a8d1365b77f4/40,-75?exclude=minutely,hourly&lang=en&units=si';

const request = https.request(url, (response) => {
  let data = '';

  // Runs multiple times...
  response.on('data', (chunk) => {
    data = data + chunk.toString();
  });

  response.on('end', () => {
    const body = JSON.parse(data);
    console.log(body);
  });
});

request.on('error', (error) => {
  console.log(error);
});
request.end();
