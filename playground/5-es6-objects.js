// Object property shorthand

const name = 'Uziel';
const userAge = 34;

const user = {
  name,
  age: userAge,
  location: '',
};

console.log(user);

// Object Destructuring

const product = {
  label: 'Red notebook',
  price: 3,
  stock: 201,
  salePice: undefined,
  attributes: {
    color: 'red',
    size: 'medium',
  },
};

const {
  label,
  price,
  stock,
  salePrice,
  attributes: attrs, // -> Renaming variable
  attributes: { color } = { color: 'unknown', size: 'unknown' }, // -> Destructuring nested object with default value
  rating = 'N/A', // -> Default value for undefined property in object
} = product;

console.log(label, price, stock, salePrice, attrs, rating, color);

const transaction = (type, { label, price, stock }) => {
  console.log(type, label, stock, price);
};

transaction('order', product);
