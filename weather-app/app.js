const chalk = require('chalk');
const geocode = require('./utils/geocode');
const forecast = require('./utils/forecast');

const address = process.argv[2];

if (!address) {
  return console.log(chalk.bgRed.white('Please provide an address.'));
}

geocode(address, (error, { latitude, longitude, location } = {}) => {
  if (error) {
    return console.log(chalk.white.bgRed(error));
  }

  forecast(
    latitude,
    longitude,
    (error, { summary, temperature, precipProbability } = {}) => {
      if (error) {
        return console.log(chalk.white.bgRed(error));
      }

      console.log(chalk.greenBright(location));
      console.log(
        chalk.yellow(
          summary +
            ' It is currently ' +
            temperature +
            ' degrees out. There is a ' +
            precipProbability +
            '% change of rain.'
        )
      );
    }
  );
});

// const req = https.request(
//   url,
//   {
//     method: 'GET',
//     headers: {
//       Accept: 'application/json',
//     },
//   },
//   (res) => {
//     console.log('statusCode:', res.statusCode);
//     console.log('headers:', res.headers);

//     res.on('data', (d) => {
//       console.log(JSON.parse(d));
//     });
//   }
// );

// req.on('error', (e) => {
//   console.error(e);
// });

// req.end();
