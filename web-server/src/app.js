const path = require('path');
const fs = require('fs');
const express = require('express');
const hbs = require('hbs');
const geocode = require('./utils/geocode');
const forecast = require('./utils/forecast');

const app = express();
const port = process.env.PORT || 3000;

// Define paths for Express config...
const publicDirectoryPath = path.join(__dirname, '..', 'public');
const viewsPath = path.join(__dirname, '..', 'templates', 'views');
const partialsPath = path.join(__dirname, '..', 'templates', 'partials');

// Setup handlebars engine and views location...
app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);

// Setup static directory to serve...
app.use(express.static(publicDirectoryPath));

app.get('', (req, res) => {
  res.render('index', {
    title: 'Weather',
    name: 'Uziel Bueno',
  });
});

app.get('/about', (req, res) => {
  res.render('about', {
    title: 'About',
    name: 'Uziel Bueno',
  });
});

app.get('/help', (req, res) => {
  res.render('help', {
    message: 'Hello! Are you lost?',
    title: 'Help',
    name: 'Uziel Bueno',
  });
});

app.get('/weather', (req, res) => {
  if (!req.query.address) {
    return res.status(400).send({ error: 'You must provide an address.' });
  }

  geocode(
    req.query.address,
    (error, { latitude, longitude, location } = {}) => {
      if (error) {
        return res.status(400).send({ error });
      }

      forecast(latitude, longitude, (error, forecast) => {
        if (error) {
          return res.status(400).send({ error });
        }

        res.send({
          address: req.query.address,
          location,
          forecast,
        });
      });
    }
  );
});

app.get('/products', (req, res) => {
  console.log(req.query.search);
  console.log(req.query.rating);

  if (!req.query.search) {
    return res.status(400).send({ error: 'You must provide a search term.' });
  }

  try {
    const dataBuffer = fs.readFileSync(path.join(__dirname, 'products.json'));
    const productsData = dataBuffer.toString();
    const products = JSON.parse(productsData);

    return res.send(
      products
        .filter(({ label }) => label.includes(req.query.search))
        .filter(({ rating }) =>
          req.query.rating ? rating === parseInt(req.query.rating, 10) : true
        )
    );
  } catch (e) {
    return res.status(500).send({ error: 'Could not load stored products!' });
  }
});

// Wildcard route matching...
app.get('/help/*', (req, res) => {
  res.render('404', {
    title: '404',
    name: 'Uziel Bueno',
    errorMessage: 'Help article not found!',
  });
});

app.get('*', (req, res) => {
  res.render('404', {
    title: '404',
    name: 'Uziel Bueno',
    errorMessage: 'Page not found!',
  });
});

app.listen(port, () =>
  console.log('Application listening on port ' + port + '.')
);
