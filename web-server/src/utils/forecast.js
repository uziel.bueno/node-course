const request = require('request');

const forecast = (latitude, longitude, callback) => {
  const url =
    'https://api.darksky.net/forecast/42efd423d0580866b886a8d1365b77f4/' +
    encodeURIComponent(latitude) +
    ',' +
    encodeURIComponent(longitude) +
    '?exclude=minutely,hourly&lang=en&units=si';

  request({ url, json: true }, (error, { body }) => {
    if (error) {
      return callback('Unable to connect to weather service.', undefined);
    }

    if (body.error) {
      return callback('Unable to find location: ' + body.error, undefined);
    }

    callback(
      undefined,
      body.daily.data[0].summary +
        ' It is currently ' +
        body.currently.temperature +
        ' degrees out. The High today is ' +
        body.daily.data[0].temperatureHigh +
        ' and the Low is ' +
        body.daily.data[0].temperatureLow +
        '. There is a ' +
        body.currently.precipProbability +
        '% change of rain.'
    );
  });
};

module.exports = forecast;
