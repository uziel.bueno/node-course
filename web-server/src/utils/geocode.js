const request = require('request');

const geocode = (address, callback) => {
  const url =
    'https://api.mapbox.com/geocoding/v5/mapbox.places/' +
    encodeURIComponent(address) +
    '.json?access_token=pk.eyJ1IjoidXppZWwtYnVlbm8iLCJhIjoiY2s3azNyMTJuMGQ4YTNoazFjaGd6Mm1oeCJ9.lK_akjFWjvrCY6ecz0KT2g&limit=1';

  request({ url, json: true }, (error, response, { features }) => {
    if (error) {
      return callback('Unable to connect to location services.', undefined);
    }

    if (features.length === 0) {
      return callback('Cannot find location. Try another search.', undefined);
    }

    callback(undefined, {
      latitude: features[0].center[1],
      longitude: features[0].center[0],
      location: features[0].place_name,
    });
  });
};

module.exports = geocode;
