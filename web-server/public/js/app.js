console.log('Client side javascript file is loaded');

const weatherForm = document.querySelector('form');
const submitButton = document.getElementById('submit-button');
const messageOne = document.getElementById('messageOne');
const messageTwo = document.getElementById('messageTwo');

weatherForm.addEventListener('submit', onSearch);

function onSearch(e) {
  e.preventDefault();

  messageOne.textContent = '';
  messageTwo.textContent = '';

  if (!e.target.elements.address.value) {
    messageOne.textContent = 'You must provide an address!';
    return;
  }

  submitButton.setAttribute('disabled', true);
  messageTwo.textContent = 'Loading...';

  forecast(e.target.elements.address.value)
    .then((data) => {
      if (data.error) {
        messageTwo.textContent = data.error;
        return;
      }

      messageOne.textContent = data.location;
      messageTwo.textContent = data.forecast;
    })
    .catch((error) => (messageTwo.textContent = error))
    .finally(() => submitButton.removeAttribute('disabled'));
}

const forecast = (address) => {
  return fetch('/weather?address=' + encodeURIComponent(address))
    .then((response) => {
      return response.json().catch((error) => console.error(error));
    })
    .catch((error) => console.error(error));
};
